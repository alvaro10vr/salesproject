package com.sales.service.app.model.domain;

/**
 * @author Alvaro Veliz
 */
public enum PersonGender {
    MALE,
    FEMALE
}
