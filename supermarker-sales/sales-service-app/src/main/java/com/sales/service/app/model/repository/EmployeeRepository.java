package com.sales.service.app.model.repository;

import com.sales.service.app.model.domain.Employee;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;


/**
 * @author Alvaro Veliz
 */
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    @Query("select item from Employee item where item.email = :email")
    Optional<Employee> findByEmail(@Param("email") String email);

    @Query("select item from Employee item where item.email = ?1")
    Optional<Employee> findEmployeeByEmail(String email);
}
