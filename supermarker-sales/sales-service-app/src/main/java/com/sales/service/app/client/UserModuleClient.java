package com.sales.service.app.client;


import com.sales.service.app.input.SalesContactInput;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author Alvaro Veliz
 */
@FeignClient(name = "contact-service")
interface UserModuleClient {

    @RequestMapping(
            value = "/secure/contacts/createSalesContact",
            method = RequestMethod.GET
    )
    SalesContactInput createSalesContact(@RequestBody SalesContactInput input);
}
