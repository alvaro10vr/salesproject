package com.sales.service.app.model.domain;

import javax.persistence.*;

/**
 * @author Alvaro Veliz
 */
@Entity
@Table(name = "Detail")
public class Detail {

    @Id
    @Column(name = "detailid", nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "totalProducts")
    private Integer totalProducts;

    @Column(name = "totalPrice")
    private Long totalPrice;

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "saledetailid", referencedColumnName = "saleid", nullable = false)
    private Sale sale;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTotalProducts() {
        return totalProducts;
    }

    public void setTotalProducts(Integer totalProducts) {
        this.totalProducts = totalProducts;
    }

    public Long getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Long totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Sale getSale() {
        return sale;
    }

    public void setSale(Sale sale) {
        this.sale = sale;
    }
}
